/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-adapter.h
 */

#pragma once

#include "mqm-executor.h"
#include "mqm-options.h"
#include "mqm-types.h"

#include <glib.h>
#include <mosquitto.h>
#include <pthread.h>
#include <stdbool.h>

G_BEGIN_DECLS

/**
 * @enum adapter_event_data
 * @brief Adapter event data type
 */
typedef enum _AdapterEventType {
    ADAPTER_EVENT_MQTT_START,
    ADAPTER_EVENT_MQTT_STOP
} AdapterEventType;

typedef gboolean (*MqmAdapterCallback)(gpointer _adapter, gpointer _event);

typedef void (*MqmAdapterAvailableCb)(gpointer _dbus_proxy, gpointer _data);

/**
 * @struct MqmAdapterEvent
 */
typedef struct _MqmAdapterEvent {
    AdapterEventType type; /**< The event type the element holds */
} MqmAdapterEvent;

typedef struct _MqmAdapter {
    GSource source;              /**< Event loop source */
    GAsyncQueue *queue;          /**< Async queue */
    MqmAdapterCallback callback; /**< Callback function */
    MqmOptions *options;
    MqmExecutor *executor;

    pthread_t thread;
    gboolean thread_stop;
    struct mosquitto *mosq;
    gchar *client_id;

    grefcount rc; /**< Reference counter variable  */
} MqmAdapter;

/*
 * @brief Create a new adapter object
 * @return On success return a new MqmAdapter object
 */
MqmAdapter *mqm_adapter_new(MqmOptions *options, MqmExecutor *executor);

/**
 * @brief Aquire adapter object
 * @return The referenced adapter object
 */
MqmAdapter *mqm_adapter_ref(MqmAdapter *adapter);

/**
 * @brief Release adapter object
 */
void mqm_adapter_unref(MqmAdapter *adapter);

/**
 * @brief Start MQTT
 */
void mqm_adapter_mqtt_start(MqmAdapter *adapter);

/**
 * @brief Stop MQTT
 */
void mqm_adapter_mqtt_stop(MqmAdapter *adapter);

/**
 * @brief Set switch state
 * @param adapter Pointer to the adapter object
 * @param switch_state switch state as string
 */
void mqm_adapter_set_switch_state(MqmAdapter *adapter, const char *switch_state);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(MqmAdapter, mqm_adapter_unref);

G_END_DECLS
