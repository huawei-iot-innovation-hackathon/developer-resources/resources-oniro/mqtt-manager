/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-adapter.c
 */

#include "mqm-adapter.h"
#include "mqm-defaults.h"

static const gchar *mqm_smartlamp_switch_message = "/huawei/smartlamp/switch";

/**
 * @brief Post new event
 * @param adapter A pointer to the adapter object
 * @param type The type of the new event to be posted
 */
static void post_adapter_event(MqmAdapter *adapter, AdapterEventType type);
/**
 * @brief GSource prepare function
 */
static gboolean adapter_source_prepare(GSource *source, gint *timeout);
/**
 * @brief GSource dispatch function
 */
static gboolean adapter_source_dispatch(GSource *source, GSourceFunc callback, gpointer _adapter);
/**
 * @brief GSource callback function
 */
static gboolean adapter_source_callback(gpointer _adapter, gpointer _event);
/**
 * @brief GSource destroy notification callback function
 */
static void adapter_source_destroy_notify(gpointer _adapter);
/**
 * @brief Async queue destroy notification callback function
 */
static void adapter_queue_destroy_notify(gpointer _adapter);
/**
 * @brief Lunch mosquitto thread
 */
static void adapter_mqtt_start(MqmAdapter *adapter);
/**
 * @brief Stop mosquitto thread
 */
static void adapter_mqtt_stop(MqmAdapter *adapter);
/**
 * @brief MQTT Connect callback
 */
void mqtt_connect_callback(struct mosquitto *mosq, void *obj, int result);
/**
 * @brief MQTT Message callback
 */
void mqtt_message_callback(struct mosquitto *mosq,
                           void *obj,
                           const struct mosquitto_message *message);

/**
 * @brief GSourceFuncs vtable
 */
static GSourceFuncs adapter_source_funcs = {
    adapter_source_prepare,
    NULL,
    adapter_source_dispatch,
    NULL,
    NULL,
    NULL,
};

static void post_adapter_event(MqmAdapter *adapter, AdapterEventType type)
{
    MqmAdapterEvent *e = NULL;

    g_assert(adapter);

    e = g_new0(MqmAdapterEvent, 1);
    e->type = type;
    g_async_queue_push(adapter->queue, e);
}

static gboolean adapter_source_prepare(GSource *source, gint *timeout)
{
    MqmAdapter *adapter = (MqmAdapter *) source;

    MQM_UNUSED(timeout);

    return (g_async_queue_length(adapter->queue) > 0);
}

static gboolean adapter_source_dispatch(GSource *source, GSourceFunc callback, gpointer _adapter)
{
    MqmAdapter *adapter = (MqmAdapter *) source;
    gpointer event = g_async_queue_try_pop(adapter->queue);

    MQM_UNUSED(callback);
    MQM_UNUSED(_adapter);

    if (event == NULL)
        return G_SOURCE_CONTINUE;

    return adapter->callback(adapter, event) == TRUE ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
}

static gboolean adapter_source_callback(gpointer _adapter, gpointer _event)
{
    MqmAdapter *adapter = (MqmAdapter *) _adapter;
    MqmAdapterEvent *event = (MqmAdapterEvent *) _event;

    g_assert(adapter);
    g_assert(event);

    switch (event->type) {
    case ADAPTER_EVENT_MQTT_START:
        adapter_mqtt_start(adapter);
        break;
    case ADAPTER_EVENT_MQTT_STOP:
        adapter_mqtt_stop(adapter);
        break;
    default:
        break;
    }

    g_free(event);

    return TRUE;
}

static void adapter_source_destroy_notify(gpointer _adapter)
{
    MqmAdapter *adapter = (MqmAdapter *) _adapter;

    g_assert(adapter);
    g_debug("Adapter destroy notification");

    mqm_adapter_unref(adapter);
}

static void adapter_queue_destroy_notify(gpointer _adapter)
{
    MQM_UNUSED(_adapter);
    g_debug("Adapter queue destroy notification");
}

void mqtt_connect_callback(struct mosquitto *mosq, void *obj, int result)
{
    g_info("MQTT connect callback, result=%d", result);
    mosquitto_subscribe(mosq, NULL, mqm_smartlamp_switch_message, 0);
}

void mqtt_message_callback(struct mosquitto *mosq,
                           void *obj,
                           const struct mosquitto_message *message)
{
    MqmAdapter *adapter = (MqmAdapter *) obj;
    bool match = false;

    g_assert(adapter);

    mosquitto_topic_matches_sub(mqm_smartlamp_switch_message, message->topic, &match);
    if (match) {
        g_autofree gchar *lamp_state = NULL;
        gchar tmp[128] = {0};

        memcpy(tmp, message->payload, message->payloadlen);

        if (g_strcmp0(tmp, MQM_LAMP_STATE_ON) == 0)
            lamp_state = g_strdup(MQM_LAMP_STATE_ON);
        else
            lamp_state = g_strdup(MQM_LAMP_STATE_OFF);

        {
            ExecutorActionPayload pld = {.lamp_state = g_strdup(lamp_state)};
            g_debug("Push new executor event for new_lamp_state='%s", lamp_state);
            mqm_executor_push_event(adapter->executor, EXECUTOR_UPDATE_LAMP_STATE, pld);
        }
    }
}

static void *mqtt_main_thread(void *arg)
{
    MqmAdapter *adapter = mqm_adapter_ref((MqmAdapter *) arg);
    g_autofree gchar *host_addr = NULL;
    long host_port = -1;
    gint rc = 0;

    g_assert(adapter);

    host_addr = mqm_options_string_for(adapter->options, KEY_BROKER_ADDRESS);
    host_port = mqm_options_long_for(adapter->options, KEY_BROKER_PORT);

    rc = mosquitto_connect(adapter->mosq, host_addr, host_port, 60);
    while (!adapter->thread_stop) {
        rc = mosquitto_loop(adapter->mosq, -1, 1);
        if (!adapter->thread_stop && rc) {
            g_info("MQTT connection error");
            sleep(5);
            mosquitto_reconnect(adapter->mosq);
        }
    }

    mqm_adapter_unref(adapter);

    return NULL;
}

static void adapter_mqtt_start(MqmAdapter *adapter)
{
    gint status = 0;

    g_assert(adapter);

    status = pthread_create(&adapter->thread, NULL, &mqtt_main_thread, adapter);
    g_assert(status == 0);
}

static void adapter_mqtt_stop(MqmAdapter *adapter)
{
    g_assert(adapter);
    adapter->thread_stop = TRUE;
    (void) pthread_join(adapter->thread, NULL);
}

MqmAdapter *mqm_adapter_new(MqmOptions *options, MqmExecutor *executor)
{
    MqmAdapter *adapter = (MqmAdapter *) g_source_new(&adapter_source_funcs, sizeof(MqmAdapter));

    g_assert(adapter);
    g_assert(executor);

    g_ref_count_init(&adapter->rc);
    adapter->executor = mqm_executor_ref(executor);
    adapter->queue = g_async_queue_new_full(adapter_queue_destroy_notify);
    adapter->callback = adapter_source_callback;
    adapter->options = mqm_options_ref(options);

    mosquitto_lib_init();

    adapter->client_id = g_strdup_printf("mqm_log_%d", getpid());
    adapter->mosq = mosquitto_new(adapter->client_id, true, 0);
    if (adapter->mosq == NULL)
        g_error("Cannot create mosquitto client");
    g_assert(adapter->mosq);

    mosquitto_user_data_set(adapter->mosq, adapter);
    mosquitto_connect_callback_set(adapter->mosq, mqtt_connect_callback);
    mosquitto_message_callback_set(adapter->mosq, mqtt_message_callback);

    g_source_set_callback(MQM_EVENT_SOURCE(adapter), NULL, adapter, adapter_source_destroy_notify);
    g_source_attach(MQM_EVENT_SOURCE(adapter), NULL);

    return adapter;
}

MqmAdapter *mqm_adapter_ref(MqmAdapter *a)
{
    g_assert(a);
    g_ref_count_inc(&a->rc);
    return a;
}

void mqm_adapter_unref(MqmAdapter *a)
{
    g_assert(a);

    if (g_ref_count_dec(&a->rc) == TRUE) {
        if (a->options != NULL)
            mqm_options_unref(a->options);

        if (a->executor != NULL)
            mqm_executor_unref(a->executor);

        if (a->mosq != NULL)
            mosquitto_destroy(a->mosq);

        mosquitto_lib_cleanup();
        g_free(a->client_id);

        g_async_queue_unref(a->queue);
        g_source_unref(MQM_EVENT_SOURCE(a));
    }
}

void mqm_adapter_set_switch_state(MqmAdapter *adapter, const char *switch_state)
{
    g_assert(adapter);
    g_assert(switch_state);

    g_debug("Report new switch state to MQTT LampState='%s'", switch_state);
    mosquitto_publish(adapter->mosq,
                      NULL,
                      mqm_smartlamp_switch_message,
                      strlen(switch_state),
                      switch_state,
                      0,
                      0);
}

void mqm_adapter_mqtt_start(MqmAdapter *adapter)
{
    post_adapter_event(adapter, ADAPTER_EVENT_MQTT_START);
}

void mqm_adapter_mqtt_stop(MqmAdapter *adapter)
{
    post_adapter_event(adapter, ADAPTER_EVENT_MQTT_STOP);
}
