/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-dbusown.h
 */

#pragma once

#include "mqm-executor.h"
#include "mqm-options.h"
#include "mqm-types.h"

#include <gio/gio.h>
#include <glib.h>

G_BEGIN_DECLS

/**
 * @enum dbusown_event_data
 */
typedef enum _DBusOwnEventType { DBUS_OWN_EMIT_NEW_LAMP_STATE } DBusOwnEventType;

typedef gboolean (*MqmDBusOwnCallback)(gpointer _dbusown, gpointer _event);

/**
 * @struct MqmDBusOwnEvent
 */
typedef struct _MqmDBusOwnEvent {
    DBusOwnEventType type; /**< The event type the element holds */
    gchar *lamp_state;
} MqmDBusOwnEvent;

/**
 * @struct MqmDBusOwn
 * @brief The MqmDBusOwn opaque data structure
 */
typedef struct _MqmDBusOwn {
    GSource source;              /**< Event loop source */
    GAsyncQueue *queue;          /**< Async queue */
    MqmDBusOwnCallback callback; /**< Callback function */
    grefcount rc;                /**< Reference counter variable  */
    MqmOptions *options;         /**< Reference options object */
    MqmExecutor *executor;       /**< Reference executor object */
    guint owner_id;              /**< DBUS owner id */
    GDBusConnection *connection; /**< DBUS connection */
} MqmDBusOwn;

/*
 * @brief Create a new dbusown object
 * @return On success return a new MqmDBusOwn object otherwise return NULL
 */
MqmDBusOwn *mqm_dbusown_new(MqmOptions *options, MqmExecutor *executor);

/**
 * @brief Aquire dbusown object
 */
MqmDBusOwn *mqm_dbusown_ref(MqmDBusOwn *d);

/**
 * @brief Release dbusown object
 */
void mqm_dbusown_unref(MqmDBusOwn *d);

/**
 * @brief Build DBus proxy
 */
void mqm_dbusown_emit_new_lamp_state(MqmDBusOwn *d, const gchar *lamp_state);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(MqmDBusOwn, mqm_dbusown_unref);

G_END_DECLS
