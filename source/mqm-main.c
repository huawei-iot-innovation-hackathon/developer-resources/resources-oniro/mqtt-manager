/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-main.c
 */

#include "mqm-application.h"
#include "mqm-logging.h"

#include <fcntl.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

gboolean g_lamp_state = FALSE;

static GMainLoop *g_mainloop = NULL;
static MqmApplication *g_app = NULL;

static void terminate(int signum)
{
    g_info("MQTT-Manager terminate with signal %d", signum);

    if (g_app != NULL)
        if (g_app->adapter != NULL)
            mqm_adapter_mqtt_stop(g_app->adapter);

    if (g_mainloop != NULL)
        g_main_loop_quit(g_mainloop);
}

gint main(gint argc, gchar *argv[])
{
    g_autoptr(GOptionContext) context = NULL;
    g_autoptr(GError) error = NULL;
    g_autoptr(MqmApplication) app = NULL;
    g_autofree gchar *config_path = NULL;
    gboolean version = FALSE;
    MqmStatus status = MQM_STATUS_OK;

    GOptionEntry main_entries[] = {
        {"version", 'v', 0, G_OPTION_ARG_NONE, &version, "Show program version", ""},
        {"config", 'c', 0, G_OPTION_ARG_FILENAME, &config_path, "Override configuration file", ""},
        {0}};

    signal(SIGINT, terminate);
    signal(SIGTERM, terminate);

    context = g_option_context_new("- MQTT-Manager service daemon");
    g_option_context_set_summary(context, "The service is managing MQTT cloud state");
    g_option_context_add_main_entries(context, main_entries, NULL);

    if (!g_option_context_parse(context, &argc, &argv, &error)) {
        g_printerr("%s\n", error->message);
        return EXIT_FAILURE;
    }

    if (version) {
        g_printerr("%s\n", MQM_VERSION);
        return EXIT_SUCCESS;
    }

    mqm_logging_open("MQM", "MQTT Manager Service", "MQM", "Default context");

    if (config_path == NULL)
        config_path = g_build_filename(MQM_CONFIG_DIRECTORY, MQM_CONFIG_FILE_NAME, NULL);

    if (g_access(config_path, R_OK) == 0) {
        g_app = app = mqm_application_new(config_path, &error);
        if (error != NULL) {
            g_printerr("%s\n", error->message);
            status = MQM_STATUS_ERROR;
        } else {
            g_info("MQTT manager service started");
            g_mainloop = mqm_application_get_mainloop(app);
            status = mqm_application_execute(app);
        }
    } else
        g_warning("Cannot open configuration file %s", config_path);

    mqm_logging_close();

    return status == MQM_STATUS_OK ? EXIT_SUCCESS : EXIT_FAILURE;
}
