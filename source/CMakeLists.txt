set(BINARY_NAME mqtt-manager)

set(BINARY_SRC 
    mqm-main.c
    mqm-application.c
    mqm-dbusown.c
    mqm-executor.c
    mqm-sdnotify.c
    mqm-options.c
    mqm-adapter.c
    mqm-logging.c)

add_executable(${BINARY_NAME} ${BINARY_SRC})
target_link_libraries(${BINARY_NAME} ${LIBS} Threads::Threads)

install(TARGETS ${BINARY_NAME} RUNTIME DESTINATION bin)
