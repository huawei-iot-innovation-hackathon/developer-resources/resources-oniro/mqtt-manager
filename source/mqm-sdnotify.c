/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-sdnotify.c
 */

#include "mqm-sdnotify.h"

#include <sys/timerfd.h>
#include <systemd/sd-daemon.h>

#define USEC2SEC(x) (x / 1000000)
#define USEC2SECHALF(x) (guint)(x / 1000000 / 2)

/**
 * @brief GSource callback function
 */
static gboolean source_timer_callback(gpointer data);
/**
 * @brief GSource destroy notification callback function
 */
static void source_destroy_notify(gpointer data);

static gboolean source_timer_callback(gpointer data)
{
    MQM_UNUSED(data);

    if (sd_notify(0, "WATCHDOG=1") < 0)
        g_warning("Fail to send the heartbeet to systemd");
    else
        g_debug("Watchdog heartbeat sent");

    return TRUE;
}

static void source_destroy_notify(gpointer data)
{
    MQM_UNUSED(data);
    g_info("System watchdog disabled");
}

MqmSDNotify *mqm_sdnotify_new(void)
{
    MqmSDNotify *sdnotify = g_new0(MqmSDNotify, 1);
    gint sdw_status;
    gulong usec = 0;

    g_assert(sdnotify);

    g_ref_count_init(&sdnotify->rc);

    sdw_status = sd_watchdog_enabled(0, &usec);

    if (sdw_status > 0) {
        g_info("Systemd watchdog enabled with timeout %lu seconds", USEC2SEC(usec));

        sdnotify->source = g_timeout_source_new_seconds(USEC2SECHALF(usec));
        g_source_ref(sdnotify->source);

        g_source_set_callback(sdnotify->source,
                              G_SOURCE_FUNC(source_timer_callback),
                              sdnotify,
                              source_destroy_notify);
        g_source_attach(sdnotify->source, NULL);
    } else {
        if (sdw_status == 0)
            g_info("Systemd watchdog disabled");
        else
            g_warning("Fail to get the systemd watchdog status");
    }

    return sdnotify;
}

MqmSDNotify *mqm_sdnotify_ref(MqmSDNotify *sdnotify)
{
    g_assert(sdnotify);
    g_ref_count_inc(&sdnotify->rc);
    return sdnotify;
}

void mqm_sdnotify_unref(MqmSDNotify *sdnotify)
{
    g_assert(sdnotify);

    if (g_ref_count_dec(&sdnotify->rc) == TRUE) {
        if (sdnotify->source != NULL)
            g_source_unref(sdnotify->source);

        g_free(sdnotify);
    }
}
