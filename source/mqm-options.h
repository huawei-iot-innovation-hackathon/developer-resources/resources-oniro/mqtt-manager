/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-options.h
 */

#pragma once

#include "mqm-types.h"

#include <glib.h>

G_BEGIN_DECLS

/**
 * @enum Option keys
 */
typedef enum _MqmOptionsKey { KEY_BROKER_ADDRESS, KEY_BROKER_PORT } MqmOptionsKey;

/**
 * @struct Option object
 */
typedef struct _MqmOptions {
    GKeyFile *conf;    /**< The GKeyFile object */
    gboolean has_conf; /**< True if a runtime option object is available */
    grefcount rc;      /**< Reference counter variable  */
} MqmOptions;

/*
 * @brief Create a new options object
 */
MqmOptions *mqm_options_new(const gchar *conf_path);

/*
 * @brief Aquire options object
 */
MqmOptions *mqm_options_ref(MqmOptions *opts);

/**
 * @brief Release an options object
 */
void mqm_options_unref(MqmOptions *opts);

/**
 * @brief Get the GKeyFile object
 */
GKeyFile *mqm_options_get_key_file(MqmOptions *opts);

/*
 * @brief Get a configuration value string for key
 */
gchar *mqm_options_string_for(MqmOptions *opts, MqmOptionsKey key);

/*
 * @brief Get a configuration gint64 value for key
 */
gint64 mqm_options_long_for(MqmOptions *opts, MqmOptionsKey key);

G_END_DECLS
