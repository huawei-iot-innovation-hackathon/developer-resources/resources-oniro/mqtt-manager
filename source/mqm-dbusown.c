/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-dbusown.c
 */

#include "mqm-dbusown.h"
#include <gio/gio.h>

extern gboolean g_lamp_switch;

/**
 * @brief Post new event
 */
static void post_dbusown_event(MqmDBusOwn *d, MqmDBusOwnEvent *e);
/**
 * @brief GSource prepare function
 */
static gboolean dbusown_source_prepare(GSource *source, gint *timeout);
/**
 * @brief GSource dispatch function
 */
static gboolean dbusown_source_dispatch(GSource *source, GSourceFunc callback, gpointer _dbusown);
/**
 * @brief GSource callback function
 */
static gboolean dbusown_source_callback(gpointer _dbusown, gpointer _event);
/**
 * @brief GSource destroy notification callback function
 */
static void dbusown_source_destroy_notify(gpointer _dbusown);
/**
 * @brief Async queue destroy notification callback function
 */
static void dbusown_queue_destroy_notify(gpointer _dbusown);
/**
 * @brief Handle new new lamp state emit request
 */
static void dbusown_emit_new_lamp_state(MqmDBusOwn *d, const gchar *lamp_state);
/**
 * @brief Handle method call
 */
static void handle_method_call(GDBusConnection *connection,
                               const gchar *sender,
                               const gchar *object_path,
                               const gchar *interface_name,
                               const gchar *method_name,
                               GVariant *parameters,
                               GDBusMethodInvocation *invocation,
                               gpointer user_data);
/**
 * @brief Handle get property
 */
static GVariant *handle_get_property(GDBusConnection *connection,
                                     const gchar *sender,
                                     const gchar *object_path,
                                     const gchar *interface_name,
                                     const gchar *property_name,
                                     GError **error,
                                     gpointer user_data);
/**
 * @brief Handle set property
 */
static gboolean handle_set_property(GDBusConnection *connection,
                                    const gchar *sender,
                                    const gchar *object_path,
                                    const gchar *interface_name,
                                    const gchar *property_name,
                                    GVariant *value,
                                    GError **error,
                                    gpointer user_data);
/**
 * @brief On DBUS acquired
 */
static void on_bus_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data);
/**
 * @brief On DBUS name acquired
 */
static void on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data);
/**
 * @brief On DBUS name lost
 */
static void on_name_lost(GDBusConnection *connection, const gchar *name, gpointer user_data);

/**
 * @brief GSourceFuncs vtable
 */
static GSourceFuncs dbusown_source_funcs = {
    dbusown_source_prepare,
    NULL,
    dbusown_source_dispatch,
    NULL,
    NULL,
    NULL,
};

/**
 * @brief GDBUS interface vtable
 */
static const GDBusInterfaceVTable interface_vtable
    = {handle_method_call, handle_get_property, handle_set_property, {0, 0, 0, 0, 0, 0, 0, 0}};

static GDBusNodeInfo *introspection_data = NULL;

/* Introspection data for the service we are exporting */
static const gchar introspection_xml[]
    = "<node>"
      "  <interface name='com.huawei.mqttmanager.Lamp'>"
      "    <signal name='NewLampState'>"
      "      <annotation name='com.huawei.mqttmanager.Annotation' value='Onsignal'/>"
      "      <arg type='s' name='lamp_state'/>"
      "    </signal>"
      "    <property type='s' name='LampState' access='readwrite'/> "
      "    <method name='setLampState'>"
      "      <arg name='request' type='s' direction='in'/>"
      "    </method>"
      "  </interface>"
      "</node>";

static void post_dbusown_event(MqmDBusOwn *d, MqmDBusOwnEvent *e)
{
    g_assert(d);
    g_assert(e);

    g_async_queue_push(d->queue, e);
}

static gboolean dbusown_source_prepare(GSource *source, gint *timeout)
{
    MqmDBusOwn *d = (MqmDBusOwn *) source;

    MQM_UNUSED(timeout);

    return (g_async_queue_length(d->queue) > 0);
}

static gboolean dbusown_source_dispatch(GSource *source, GSourceFunc callback, gpointer _dbusown)
{
    MqmDBusOwn *d = (MqmDBusOwn *) source;
    gpointer e = g_async_queue_try_pop(d->queue);

    MQM_UNUSED(callback);
    MQM_UNUSED(_dbusown);

    if (e == NULL)
        return G_SOURCE_CONTINUE;

    return d->callback(d, e) == TRUE ? G_SOURCE_CONTINUE : G_SOURCE_REMOVE;
}

static gboolean dbusown_source_callback(gpointer _dbusown, gpointer _event)
{
    MqmDBusOwn *d = (MqmDBusOwn *) _dbusown;
    MqmDBusOwnEvent *e = (MqmDBusOwnEvent *) _event;

    g_assert(d);
    g_assert(e);

    switch (e->type) {
    case DBUS_OWN_EMIT_NEW_LAMP_STATE:
        dbusown_emit_new_lamp_state(d, e->lamp_state);
        break;
    default:
        break;
    }

    g_free(e->lamp_state);
    g_free(e);

    return TRUE;
}

static void dbusown_source_destroy_notify(gpointer _dbusown)
{
    MqmDBusOwn *d = (MqmDBusOwn *) _dbusown;

    g_assert(d);
    g_debug("DBusOwn destroy notification");

    mqm_dbusown_unref(d);
}

static void dbusown_queue_destroy_notify(gpointer _dbusown)
{
    MQM_UNUSED(_dbusown);
    g_debug("DBusOwn queue destroy notification");
}

static void dbusown_emit_new_lamp_state(MqmDBusOwn *d, const gchar *lamp_state)
{
    GError *error = NULL;

    g_assert(d);
    g_assert(lamp_state);

    g_debug("Notify DBUS subscribers for lamp_state '%s'", lamp_state);

    if (d->connection != NULL) {
        g_dbus_connection_emit_signal(d->connection,
                                      NULL,
                                      "/com/huawei/mqttmanager",
                                      "com.huawei.mqttmanager.Lamp",
                                      "NewLampState",
                                      g_variant_new("(s)", lamp_state),
                                      &error);

        if (error != NULL)
            g_warning("Fail emit NewLampState signal. Error %s", error->message);
    }
}

static void handle_method_call(GDBusConnection *connection,
                               const gchar *sender,
                               const gchar *object_path,
                               const gchar *interface_name,
                               const gchar *method_name,
                               GVariant *parameters,
                               GDBusMethodInvocation *invocation,
                               gpointer user_data)
{
    MqmDBusOwn *d = (MqmDBusOwn *) user_data;

    MQM_UNUSED(connection);
    MQM_UNUSED(object_path);
    MQM_UNUSED(interface_name);

    g_debug("Handle Method Call sender: %s - %s", sender, method_name);

    if (g_strcmp0(method_name, "setLampState") == 0) {
        g_autofree gchar *new_lamp_state = NULL;
        g_autofree gchar *lamp_state = NULL;
        gchar *value = NULL;

        g_variant_get(parameters, "(s)", &value);
        g_dbus_method_invocation_return_value(invocation, NULL); /* no return value */
        g_debug("Received %s as argument", value);

        if (g_lamp_switch)
            lamp_state = g_strdup(MQM_LAMP_STATE_ON);
        else
            lamp_state = g_strdup(MQM_LAMP_STATE_OFF);

        if (g_strcmp0(value, MQM_LAMP_STATE_ON) == 0)
            new_lamp_state = g_strdup(MQM_LAMP_STATE_ON);
        else
            new_lamp_state = g_strdup(MQM_LAMP_STATE_OFF);

        g_debug("DBUS lamp_state='%s' new_lamp_state='%s'", lamp_state, new_lamp_state);
        if (g_strcmp0(lamp_state, new_lamp_state) != 0) {
            ExecutorActionPayload pld = {.lamp_state = g_strdup(new_lamp_state)};
            mqm_executor_push_event(d->executor, EXECUTOR_UPDATE_DBUS_LAMP_STATE, pld);
        }
    } else {
        g_warning("No method registered for \"%s\"", method_name);
        g_dbus_method_invocation_return_error(invocation,
                                              g_quark_from_string("MQTT-Manager"),
                                              99,
                                              "Server does not know method \"%s\"",
                                              method_name);
    }
}

static GVariant *handle_get_property(GDBusConnection *connection,
                                     const gchar *sender,
                                     const gchar *object_path,
                                     const gchar *interface_name,
                                     const gchar *property_name,
                                     GError **error,
                                     gpointer user_data)
{
    GVariant *ret = NULL;

    MQM_UNUSED(connection);
    MQM_UNUSED(sender);
    MQM_UNUSED(object_path);
    MQM_UNUSED(interface_name);
    MQM_UNUSED(error);
    MQM_UNUSED(user_data);

    g_debug("DBUS handle_get_property '%s'", property_name);
    if (g_strcmp0(property_name, "LampState") == 0) {
        g_autofree gchar *lamp_state = NULL;

        if (g_lamp_switch)
            lamp_state = g_strdup(MQM_LAMP_STATE_ON);
        else
            lamp_state = g_strdup(MQM_LAMP_STATE_OFF);

        ret = g_variant_new_string(lamp_state);
    }

    return ret;
}

static gboolean handle_set_property(GDBusConnection *connection,
                                    const gchar *sender,
                                    const gchar *object_path,
                                    const gchar *interface_name,
                                    const gchar *property_name,
                                    GVariant *value,
                                    GError **error,
                                    gpointer user_data)
{
    MqmDBusOwn *d = (MqmDBusOwn *) user_data;

    MQM_UNUSED(sender);
    MQM_UNUSED(error);
    MQM_UNUSED(connection);
    MQM_UNUSED(object_path);
    MQM_UNUSED(interface_name);

    g_debug("DBUS handle_set_property '%s'", property_name);

    if (g_strcmp0(property_name, "LampState") == 0) {
        g_autofree gchar *new_lamp_state = NULL;
        g_autofree gchar *lamp_state = NULL;

        if (g_lamp_switch)
            lamp_state = g_strdup(MQM_LAMP_STATE_ON);
        else
            lamp_state = g_strdup(MQM_LAMP_STATE_OFF);

        new_lamp_state = g_variant_dup_string(value, NULL);
        g_debug("DBUS lamp_state='%s' new_lamp_state='%s'", lamp_state, new_lamp_state);

        if (g_strcmp0(lamp_state, new_lamp_state) != 0) {
            ExecutorActionPayload pld = {.lamp_state = g_strdup(new_lamp_state)};
            mqm_executor_push_event(d->executor, EXECUTOR_UPDATE_DBUS_LAMP_STATE, pld);
        }
    }

    return TRUE;
}

static void on_bus_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    MqmDBusOwn *d = (MqmDBusOwn *) user_data;
    guint registration_id;

    d->connection = connection;
    registration_id = g_dbus_connection_register_object(connection,
                                                        "/com/huawei/mqttmanager",
                                                        introspection_data->interfaces[0],
                                                        &interface_vtable,
                                                        d,
                                                        /* user_data */
                                                        NULL,
                                                        /* user_data_free_func */
                                                        NULL); /* GError** */
    g_assert(registration_id > 0);
    g_debug("DBUS own bus '%s' acquired", name);
}

static void on_name_acquired(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    MQM_UNUSED(user_data);
    MQM_UNUSED(connection);
    g_debug("DBUS own name '%s' acquired", name);
}

static void on_name_lost(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
    MqmDBusOwn *d = (MqmDBusOwn *) user_data;

    MQM_UNUSED(connection);

    d->connection = NULL;
    g_debug("DBUS own name '%s' lost", name);
}

MqmDBusOwn *mqm_dbusown_new(MqmOptions *options, MqmExecutor *executor)
{
    MqmDBusOwn *d = (MqmDBusOwn *) g_source_new(&dbusown_source_funcs, sizeof(MqmDBusOwn));

    g_assert(d);
    g_assert(options);

    g_ref_count_init(&d->rc);

    d->options = mqm_options_ref(options);
    d->executor = mqm_executor_ref(executor);
    d->queue = g_async_queue_new_full(dbusown_queue_destroy_notify);
    d->callback = dbusown_source_callback;

    g_source_set_callback(MQM_EVENT_SOURCE(d), NULL, d, dbusown_source_destroy_notify);
    g_source_attach(MQM_EVENT_SOURCE(d), NULL);

    introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, NULL);
    g_assert(introspection_data != NULL);

    d->owner_id = g_bus_own_name(G_BUS_TYPE_SYSTEM,
                                 "com.huawei.mqttmanager",
                                 G_BUS_NAME_OWNER_FLAGS_NONE,
                                 on_bus_acquired,
                                 on_name_acquired,
                                 on_name_lost,
                                 d,
                                 NULL);

    return d;
}

MqmDBusOwn *mqm_dbusown_ref(MqmDBusOwn *d)
{
    g_assert(d);
    g_ref_count_inc(&d->rc);
    return d;
}

void mqm_dbusown_unref(MqmDBusOwn *d)
{
    g_assert(d);

    if (g_ref_count_dec(&d->rc) == TRUE) {
        if (d->options != NULL)
            mqm_options_unref(d->options);

        if (d->executor != NULL)
            mqm_executor_unref(d->executor);

        if (d->owner_id > 0)
            g_bus_unown_name(d->owner_id);

        if (introspection_data != NULL)
            g_dbus_node_info_unref(introspection_data);

        g_async_queue_unref(d->queue);
        g_source_unref(MQM_EVENT_SOURCE(d));
    }
}

void mqm_dbusown_emit_new_lamp_state(MqmDBusOwn *d, const gchar *lamp_state)
{
    MqmDBusOwnEvent *e = NULL;

    g_assert(d);
    g_assert(lamp_state);

    e = g_new0(MqmDBusOwnEvent, 1);
    e->type = DBUS_OWN_EMIT_NEW_LAMP_STATE;
    e->lamp_state = g_strdup(lamp_state);

    post_dbusown_event(d, e);
}
