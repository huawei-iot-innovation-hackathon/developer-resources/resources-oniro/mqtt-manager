/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-logging.h
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

/**
 * @brief Open logging subsystem
 * @param app_name Application identifier
 * @param app_desc Application description
 * @param ctx_name Context identifier
 * @param ctx_desc Context description
 */
void mqm_logging_open(const gchar *app_name,
                      const gchar *app_desc,
                      const gchar *ctx_name,
                      const gchar *ctx_desc);

/**
 * @brief Close logging system
 */
void mqm_logging_close(void);

G_END_DECLS
