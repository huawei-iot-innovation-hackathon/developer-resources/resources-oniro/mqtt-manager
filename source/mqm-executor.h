/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

 * @file mqm-executor.h
 */

#pragma once

#include "mqm-options.h"
#include "mqm-types.h"

#include <gio/gio.h>
#include <glib.h>

G_BEGIN_DECLS

/**
 * @enum executor_event_data
 */
typedef enum _ExecutorEventType {
    EXECUTOR_UPDATE_LAMP_STATE,
    EXECUTOR_UPDATE_DBUS_LAMP_STATE,
} ExecutorEventType;

/**
 * @function MqmExecutorCallback
 * @brief Custom callback used internally by MqmExecutor as source callback
 */
typedef gboolean (*MqmExecutorCallback)(gpointer _executor, gpointer _event);

/**
 * @struct ExecutorActionPayload
 * @brief The Action Event Payload
 */
typedef struct _ExecutorActionPayload {
    gchar *lamp_state; /**< The lamp state */
} ExecutorActionPayload;

/**
 * @struct MqmExecutorEvent
 * @brief The Executor Event
 */
typedef struct _MqmExecutorEvent {
    ExecutorEventType type;        /**< The event type the element holds */
    ExecutorActionPayload payload; /**< The event payload the element holds */
} MqmExecutorEvent;

/**
 * @struct MqmExecutor
 * @brief The MqmExecutor opaque data structure
 */
typedef struct _MqmExecutor {
    GSource source; /**< Event loop source */
    MqmOptions *options;
    gpointer dbusown;
    gpointer mqtt_adapter;
    GAsyncQueue *queue;           /**< Async queue */
    MqmExecutorCallback callback; /**< Callback function */
    grefcount rc;
} MqmExecutor;

/*
 * @brief Create a new executor object
 * @return On success return a new MqmExecutor object otherwise return NULL
 */
MqmExecutor *mqm_executor_new(MqmOptions *options);

/**
 * @brief Aquire executor object
 */
MqmExecutor *mqm_executor_ref(MqmExecutor *executor);

/**
 * @brief Release executor object
 */
void mqm_executor_unref(MqmExecutor *executor);

/**
 * @brief Set DBus object
 */
void mqm_executor_set_dbusown(MqmExecutor *executor, gpointer dbusown);

/**
 * @brief Set MQTTAdapter object
 */
void mqm_executor_set_mqtt_adapter(MqmExecutor *executor, gpointer mqtt_adapter);

/**
 * @brief Push an executor event
 */
void mqm_executor_push_event(MqmExecutor *executor,
                             ExecutorEventType type,
                             ExecutorActionPayload payload);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(MqmExecutor, mqm_executor_unref);

G_END_DECLS
