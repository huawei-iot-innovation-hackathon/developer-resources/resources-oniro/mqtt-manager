install(FILES ${CMAKE_BINARY_DIR}/config/mqttmanager-dbus.conf DESTINATION /etc/dbus-1/system.d)
install(FILES ${CMAKE_BINARY_DIR}/config/mqttmanager.conf DESTINATION /etc)
install(FILES ${CMAKE_BINARY_DIR}/config/mqttmanager.service DESTINATION /etc/systemd/system)
